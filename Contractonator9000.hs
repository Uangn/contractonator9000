module Contractonator9000 (contract) where

import Data.Bifunctor (first, second)
import Data.Char (toLower)
import qualified Data.Map as M
import qualified Data.Set as S

testSentence1 = "it is a beautiful day today, it would have been beautiful-er if it is would have is it is have would have it is is is would have have have is"

testSentence2 = "not us am are does is has have had did would will shall them you about because and you"

main :: IO()
main = do
    let filePath = "contractionFiles"
    contracts <- map ((\[x,y,z] -> (x,Contract y z)) . splitWhen (==','))
               . lines
              <$> readFile (filePath <> "/contracts")
    shorten <- map ((\[x,y,z] -> (x,Shorten y z)) . splitWhen (==','))
             . lines
            <$> readFile (filePath <> "/shorten")
    punctuation <- concat . lines <$> readFile (filePath <> "/punctuation")

    let contractionMap = M.fromList (contracts <> shorten)

    putStrLn . contract contractionMap $ testSentence2

    beeMovieTxt <- readFile "bee_movie_script.txt"
    writeFile "bee_movie_script_contracted.txt"
        . unlines
        . map (contract contractionMap)
        . lines
        $ beeMovieTxt

-- DATA
data ContractionResult = Shorten String String
                       | Contract String String
                       | Failed String

type ContractionMap = M.Map String ContractionResult

combineContractionResult :: (String -> String)
                         -> ContractionResult
                         -> String
combineContractionResult f (Failed x)    = f x
combineContractionResult f (Shorten x y) = f $ concatBetween "'" x y
combineContractionResult _ (Contract x y) = concatBetween "'" x y

punctuation :: S.Set Char
punctuation = S.fromList "?!.,;"

-- HELPERS
concatBetween :: String -> String -> String -> String
concatBetween s x y = x <> s <> y

fromMaybe :: a -> Maybe a -> a
fromMaybe = flip maybe id

ifSpaceAdd f ys@(x:xs) =
    if x == ' '
        then f (' ':) xs
        else f id ys

splitWhen :: Eq a => (a -> Bool) -> [a] -> [[a]]
splitWhen p s =
    case b of
        [] -> [a]
        (x:xs) -> a : splitWhen p xs
    where
    (a,b) = break p s


-- CONTRACTIONS
contract :: ContractionMap -> String -> String
contract contractionMap [] = []
contract contractionMap xs = a <> b <> contract contractionMap c
    where
    ((a,b),c) = ifSpaceAdd go xs
    go f = first ( first (contractWordWSpace contractionMap . f)
                 . break (`S.member` punctuation))
         . break (== ' ')

contractWordWSpace :: ContractionMap -> String -> String
contractWordWSpace contractionMap [] = []
contractWordWSpace contractionMap xs = ifSpaceAdd cwws xs
    where cwws f = combineContractionResult f . contractWord contractionMap


contractWord :: ContractionMap -> String -> ContractionResult
contractWord contractionMap =
    (fromMaybe . Failed)
    <*> ( (`M.lookup` contractionMap)
        . map toLower)
